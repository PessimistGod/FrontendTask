# My Awesome Website

Welcome to my awesome website! This repository contains the source code and assets for my personal website.

## Website URL

You can access my website by clicking on the following link:
[My Website](https://frontend-task-main.vercel.app/)

## Preview

Here are some preview images of my website:

![Image 1](Images/Screenshot__65_.png)
![Image 2](Images/Screenshot__66_.png)
![Image 3](Images/Screenshot__67_.png)
![Image 4](Images/Screenshot__68_.png)
![Image 5](Images/Screenshot__69_.png)
![Image 6](Images/Screenshot__70_.png)
![Image 7](Images/Screenshot__72_.png)
![Image 8](Images/Screenshot__73_.png)




## Description

This website is Made using React + Bootstrap

## How to Use

1. Clone this repository to your local machine.
2. Install Backend and React app 
3. To Start use npm start in frontend
4. To start use nodemon index.js in backend
